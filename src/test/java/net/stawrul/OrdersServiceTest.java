package net.stawrul;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

    @Mock
    EntityManager em;

    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookNotAvailable_placeOrderThrowsOutOfStockEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setPrice(20.0);
        book.setAmount(0);
        for(int i=0;10>i;i++)
        {
            order.getBooks().add(book);
        }

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }
    @Test
    public void whenNumberOfBooksIsGood() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setPrice(20.0);
        book.setAmount(30);
        for(int i=0;10>i;i++)
        {
            order.getBooks().add(book);
        }

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
        assertEquals(20, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }
    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmountByOne() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(20);
        book.setPrice(200.0);
        for(int i=0;10>i;i++)
        {
            order.getBooks().add(book);
        }
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(10, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }
    @Test(expected = priceTooLowException.class)
    public void whenOrderedBooksPriceIsTooLow_throwsExceptionPriceTooLow() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(10);
        book.setPrice(9.0);
        for(int i=0;10>i;i++)
        {
            order.getBooks().add(book);
        }
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        
    }
    @Test(expected = tooManyProductsInOrderException.class)
    public void whenTooManyProductsInOrderException_throwsExceptionTooManyProductsInOrder() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(300);
        book.setPrice(9.0);
        for(int i=0;300>i;i++)
        {
            order.getBooks().add(book);
        }
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        
    }
    @Test
    public void whenOrderedBooksPriceIsGood() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(10);
        book.setPrice(130.0);
        double price1=book.getAmount()*book.getPrice();
        double price=0;
        for(int i=0;10>i;i++)
        {
            order.getBooks().add(book);
            price=price+book.getPrice();
        }
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        double price2=book.getAmount()*book.getPrice();
        assertEquals((int)price, (int)(price1-price2));
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }
    @Test(expected = priceTooLowException.class)
    public void whenWrongNumberOfBooksException_throwsExceptionWrongNumber() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(10);
        book.setPrice(10.0);
        for(int i=0;9>i;i++)
        {
            order.getBooks().add(book);
        }
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        
    }
    
}
